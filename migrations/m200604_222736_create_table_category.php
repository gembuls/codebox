<?php

use yii\db\Migration;

/**
 * Class m200604_222736_create_table_category
 */
class m200604_222736_create_table_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%cdb_category}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(256)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cdb_category}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200604_222736_create_table_category cannot be reverted.\n";

        return false;
    }
    */
}
