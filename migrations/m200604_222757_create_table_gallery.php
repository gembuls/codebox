<?php

use yii\db\Migration;

/**
 * Class m200604_222757_create_table_gallery
 */
class m200604_222757_create_table_gallery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$this->createTable('{{%cdb_gallery}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(256),
            'description' => $this->text(),
            'file_name' => $this->text(),
            'file_size' => $this->integer(),
            'file_type' => $this->text(),
			'file_content' => $this->text(),
            'thumbnail_size' => $this->integer(),
            'thumbnail_content' => $this->text(),
            'photo_date' => $this->date(),
            'created_at' => $this->datetime(),
            'updated_at' => $this->datetime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cdb_gallery}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200604_222757_create_table_gallery cannot be reverted.\n";

        return false;
    }
    */
}
