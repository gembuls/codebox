<?php
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Photo Gallery';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row" style="height:250px">
            <div class="col-md-4">
				<img src="">
			</div>
            <div class="col-md-8">
			<br />
			<h1><b>GEMBUL'S GALLERY</b></h1>
			<p>Please enjoy my personal photo gallery.</p>
			<?php if(Yii::$app->user->id){ ?>
			<p>
				<?= Html::a('Upload Foto', ['gallery/create'], ['class' => 'btn btn-success']) ?>
			</p>
			<?php } ?>
			</div>
        </div>
		
		<div class="row" id="contentgallery">
			<?php foreach($model as $rows){ ?>
			<div class="col-md-4" style="margin-top:30px">
				<img src="<?= Url::to(['site/thumbnail', 'id' => $rows->id]) ?>" width="100%" alt="<?= $rows->file_name ?>">
			</div>
			<?php } ?>
		</div>

    </div>
</div>
<?php
$this->registerJs("
	var pages = ".$pages.";
	var load = false;
	$(window).scroll(function() {
		if($(window).scrollTop() >= $(document).height() - ($(window).height()+ 100)) {
			if(!load)
			{
				 load = true;
				  pages++;
				 $.ajax({
					 url : '".Url::to(['site/content-gallery','pages' => ''])."'+pages,
					 success: function(data)
					 {
						 if(data != '')
						 {
							 $('#contentgallery').append(data);
							 load = false;
						 }
					 }
				 })
			}
		}
	});
");
?>
