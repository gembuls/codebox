<?php
use yii\helpers\Url;
?>
<?php foreach($model as $rows){ ?>
<div class="col-md-4" style="margin-top:20px">
	<img src="<?= Url::to(['site/thumbnail', 'id' => $rows->id]) ?>" width="100%" alt="<?= $rows->file_name ?>">
</div>
<?php } ?>