<?php

namespace app\models;

use Yii;
use yii\imagine\Image;
use Imagine\Image\Box;
use yii\web\UploadedFile;

/**
 * This is the model class for table "cdb_gallery".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $description
 * @property string|null $file_name
 * @property int|null $file_size
 * @property string|null $file_type
 * @property resource|null $file_content
 * @property int|null $thumbnail_size
 * @property resource|null $thumbnail_content
 * @property string|null $photo_date
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Gallery extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
	 
	public $file_upload, $jumlah;
	
    public static function tableName()
    {
        return 'cdb_gallery';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description', 'file_name', 'file_type', 'file_content', 'thumbnail_content'], 'string'],
            [['file_size', 'thumbnail_size'], 'integer'],
            [['photo_date', 'created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 256],
            [['file_upload'], 'file','extensions'=>['jpg','jpeg','png','bmp'],'skipOnEmpty'=>true,'maxSize'=>10024000,'tooBig'=>'Max 10 MB!!'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'file_name' => 'File Name',
            'file_size' => 'File Size',
            'file_type' => 'File Type',
            'file_content' => 'File Content',
            'thumbnail_size' => 'Thumbnail Size',
            'thumbnail_content' => 'Thumbnail Content',
            'photo_date' => 'Photo Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
	
	public function beforeSave($insert)
	{
		if($file = UploadedFile::getInstance($this, 'file_upload'))
		{
			$this->file_name = $file->name;
			$this->file_size = $file->size;
			$this->file_type = $file->type;
			$this->file_content = 'gallery/'.date("YmdHis").$file->name;
			
			$this->thumbnail_content = 'thumbnail/'.date("YmdHis").$file->name;
			
			if($file->saveAs($this->file_content))
			{			
				Image::thumbnail($this->file_content, 500,500)
				->save($this->thumbnail_content, ['quality' => 100]);
				
				
				$this->thumbnail_size = filesize($this->thumbnail_content);
			} else {
				
				return false;
			}
			
		}
		
		if($this->isNewRecord)
			$this->created_at = date("Y-m-d H:i:s");
		
		$this->updated_at = date("Y-m-d H:i:s");
		
		return parent::beforeSave($insert);
	}
}
